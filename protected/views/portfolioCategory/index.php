<?php
$this->breadcrumbs=array(
	'Portfolio Categories',
);

$this->menu=array(
array('label'=>'Create PortfolioCategory','url'=>array('create')),
array('label'=>'Manage PortfolioCategory','url'=>array('admin')),
);
?>

<h1>Portfolio Categories</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
