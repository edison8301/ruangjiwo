<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'role-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php print CHtml::submitButton('Save'); ?>
	<?php /* $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); */ ?>
</div>

<?php $this->endWidget(); ?>
