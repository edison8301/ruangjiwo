<?php $page = Page::model()->findByPk(1); ?>
<h1><?php print strtoupper($page->title); ?></h1>

<div class="container">
	<div class="col-lg-2 col-md-2">&nbsp;</div>
	<div class="col-lg-8 col-md-8">
		<?php print $page->content; ?>
	</div>
	<div class="col-lg-2 col-md-2">&nbsp;</div>
</div><!-- welcome message -->

<div>&nbsp;</div>
<div>&nbsp;</div>

<div id="why-us">
	<div class="container">
		<div class="col-lg-2 col-md-2">&nbsp;</div>
		<div class="col-lg-6 col-md-6">
			<?php $whyUs = Page::model()->findByPk(9); ?>
			<?php print $whyUs->content; ?>
		</div>
		<div id="why-list" class="col-lg-4 col-md-4">
			<ul>
				<li><?php print CHtml::image(Yii::app()->baseUrl."/images/check.png"); ?> SPEED</li>
				<li><?php print CHtml::image(Yii::app()->baseUrl."/images/check.png"); ?> QUALITY</li>
				<li><?php print CHtml::image(Yii::app()->baseUrl."/images/check.png"); ?> EXPERTISE</li>
			<ul>
		</div>
	</div>
</div><!-- why us -->

<div id="latest-project">
	<div class="container" style="width:950px">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<h3 style="margin-bottom:15px">LATEST PROJECT</h3>
			</div>
		</div>
		<div>&nbsp;</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
			<?php $portfolio = Portfolio::model()->getLatestProject(null,9) ?>
			<div class="project-carousel">
			<?php foreach($portfolio as $data) { ?>
			<div class="project-item">
				<?php $referralImg = CHtml::image(Yii::app()->baseUrl."/uploads/portfolio/".$data->image."","",array("class"=>"img-circle","width"=>"250px")); 
					echo CHtml::link($referralImg,array('portfolio/read','id'=>$data->id)); ?>
				<div>&nbsp;</div>
				<div class="category-carousel"><b>Category : </b><?php print $data->PortfolioCategory->title ?></div>
				<div>&nbsp;</div>
				<div class="client-carousel"><b>Client : </b><?php print $data->client ?></div>
			</div>
			<?php } ?>
			</ul>
			</div>
			<?php $this->widget('ext.carouFredSel.ECarouFredSel', array(
					'id' => 'carousel',
					'target' => '.project-carousel',
					'config' => array(
						'auto' => array(
							'play' =>true
						),
						'items'=>3,
						'scroll'=>array(
							'fx'=>'directscroll',
							'items'=>3,
							'pauseOnHover'=>true
						),
						'pagination'=>array(
							'items'=>3
						)
					)
			)); ?>
			</div><!-- col-lg-12 -->
		</div><!-- row -->
	</div><!-- container -->
</div><!-- latest project -->

