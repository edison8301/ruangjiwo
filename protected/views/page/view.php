<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	$model->title,
);

?>

<h1>Lihat Laman</h1>

<?php print CHtml::link(Chtml::submitButton('Sunting'),array('page/update','id'=>$model->id)); ?>&nbsp;
<?php print CHtml::link(Chtml::submitButton('Kelola Page'),array('page/admin')); ?>



<?php /* $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'context'=>'primary',
		'icon'=>'pencil white',
		'url'=>array('/page/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Baca',
		'context'=>'primary',
		'icon'=>'search white',
		'url'=>array('/page/read','id'=>$model->id),
		'htmlOptions'=>array('target'=>'_blank')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/page/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola',
		'context'=>'primary',
		'icon'=>'list white',
		'url'=>array('/page/admin')
)); */ ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'type'=>'striped bordered',
	'attributes'=>array(
		'title',
		array(
			'label'=>'Content',
			'type'=>'raw',
			'value'=>$model->content
		),
		array(
			'label'=>'Url',
			'type'=>'raw',
			'value'=>$this->createUrl('page/read',array('id'=>$model->id))
		),
		'time_created',
	),
)); ?>
