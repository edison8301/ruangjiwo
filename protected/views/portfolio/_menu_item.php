<tr>
	<td style="padding-left:<?php print $level*30; ?>px">
		<?php $this->widget('booster.widgets.TbButtonGroup', array(
				'context'=>'primary',
				'size'=>'small',
				'buttons'=>array(
					array('label'=>'','items'=>array(
						array('label'=>'Update','icon'=>'pencil','url'=>array('portfolioImage/update','id'=>$data->id)),
					)),
				),
		)); ?>
		<?php print $data->title; ?></td>
	<td>
		<?php print CHtml::link('<i class="icon-pencil"></i>',array('pollingChoice/update','id'=>$data->id)); ?>
		<?php print CHtml::link('<i class="icon-trash"></i>','#',array('submit'=>array('pollingChoice/delete','id'=>$data->id),'params'=>array('returnUrl'=>$this->createUrl('polling/view',array('id'=>$model->id))),'confirm'=>'Yakin akan menghapus data?')); ?>
	</td>
</tr>