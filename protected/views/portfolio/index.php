<div class="container" style="width:950px">
	<div class="row">
		<div class="col-lg-12 col-md-12 portofolio-index">
			<h3>PORTOFOLIO</h3>
		</div>
	</div>
	<div class="row portofolio-index">
		<div class="col-lg-1 col-md-1">
			&nbsp;
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-video71"></i>',
				array('portfolio/index','category'=>1), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'VIDEOGRAPHY')) 
			?>
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-dslr"></i>',
				array('portfolio/index','category'=>2), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'PHOTOGRAPHY')) 
			?>
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-palette5"></i>',
				array('portfolio/index','category'=>3), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'GRAPHIC DESIGN')) 
			?>
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-drawing10"></i>',
				array('portfolio/index','category'=>4), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'ILLUSTRATION')) 
			?>
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-screen52"></i>',
				array('portfolio/index','category'=>5), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'WEB DESIGN')) 
			?>
		</div>
		<div class="col-lg-1 col-md-1">
			&nbsp;
		</div>
	</div>
	<div class="row portofolio-content">
			<?php $this->widget('booster.widgets.TbListView',array(
				'dataProvider'=>$dataProvider,
				'itemView'=>'_view',
			)); ?>
	</div>
</div>
<div>&nbsp;</div>
<div>&nbsp;</div>