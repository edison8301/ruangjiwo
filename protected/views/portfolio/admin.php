<?php
$this->breadcrumbs=array(
	'Portfolio'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Portfolio','url'=>array('index')),
array('label'=>'Create Portfolio','url'=>array('create')),
);
?>

<h1>Kelola Portfolio</h1>

<?php print CHtml::link(Chtml::submitButton('Tambah'),array('portfolio/create')); ?>


<?php /*$this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'primary',
			'icon'=>'plus white',
			'label'=>'Tambah',
			'url'=>array('portfolio/create')
		)); ?>&nbsp;
		
<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'primary',
			'icon'=>'list white',
			'label'=>'Kategori',
			'url'=>array('portfolioCategory/admin')
		)); */ ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'portfolio-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'title',
		array(
			'class'=>'CDataColumn',
			'name'=>'portfolio_category_id',
			'header'=>'Kategori',
			'value'=>'$data->PortfolioCategory->title',
			'filter'=>CHtml::listData(PortfolioCategory::model()->findAll(),'id','title')
		),
		'client',
		array(
			'class'=>'CDataColumn',
			'name'=>'image',
			'type'=>'raw',
			'value'=>'$data->getThumbnail(array("width"=>"400px"))'
		),
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
