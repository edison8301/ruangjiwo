<?php
$this->breadcrumbs=array(
	'User'=>array('index'),
	'Kelola',
);

$this->menu=array(
	//array('label'=>'List User','url'=>array('index')),
	//array('label'=>'Buat User','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('user-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola User</h1>

<?php print CHtml::link(Chtml::submitButton('Tambah'),array('user/create')); ?>


<?php //$this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'plus white','label'=>'Tambah','url'=>array('user/create'))); ?>&nbsp;


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'user-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered hover',
		'filter'=>$model,
		'columns'=>array(
			//'id',
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			'username',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
