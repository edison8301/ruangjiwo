<?php
$this->breadcrumbs=array(
	'Posts',
);

$this->menu=array(
	array('label'=>'Create Post','url'=>array('create')),
	array('label'=>'Manage Post','url'=>array('admin')),
);
?>

<div class="margin20" id="postCategory-read">

<h3 class="list-bottom"><?php print strtoupper($model->title); ?></h3>

<hr>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_read',
)); ?>

</div>