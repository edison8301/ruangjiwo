<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/admin/main'); ?>

<div class="row">
	<div class="col-lg-3 col-md-3">
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'items' => array(
				array('label' => 'Dashboard','icon'=>'home', 'url' => array('admin/index')),
				array('label' => 'Page','icon'=>'file', 'url' => array('page/admin')),						
				//array('label' => 'Post','icon'=>'book', 'url' => array('post/admin')),
				array('label' => 'Slide','icon'=>'play', 'url' => array('slide/admin')),
				array('label' => 'Portofolio','icon'=>'briefcase', 'url' => array('portfolio/admin')),
				array('label' => 'User','icon'=>'user', 'url' => array('user/admin')),
				array('label'=>'Logout ('.Yii::app()->user->id.')','icon'=>'off','url'=>array('site/logout'),'visible'=>!Yii::app()->user->isGuest)
			)
	)); ?>
	</div>

	<div class="col-lg-9 col-md-9">
		<div class="row">
			<div class="col-lg-12">
				<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
					<div class="alert alert-<?php print $key; ?>"><?php print $message; ?></div>
				<?php } ?>
				<?php Yii::app()->clientScript->registerScript('hideAlert',
						'$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
						CClientScript::POS_READY
				); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<?php echo $content; ?>
			</div>
		</div>
	</div><!-- content -->
</div>



<?php $this->endContent(); ?>