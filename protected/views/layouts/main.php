<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div class="container-fluid" id="page">

	<div id="header">
		<div class="row">
				<div id="logo"><?php print CHtml::image(Yii::app()->baseUrl."/images/logo.png"); ?></div>
		</div>
	</div><!-- header -->

	<div id="mainmenu">
		<div class="row">
			<?php $this->widget('zii.widgets.CMenu',array(
				'items'=>array(
					array('label'=>'ABOUT US', 'url'=>array('/site/index')),
					array('label'=>'PORTOFOLIO', 'url'=>array('/portfolio/index')),
					array('label'=>'CONTACT', 'url'=>array('/page/read','id'=>'8')),
				),
			)); ?>
		</div>
	</div><!-- mainmenu -->
	
	<?php if(Yii::app()->controller->id == 'site' AND Yii::app()->controller->action->id == 'index') { ?>
	<div id="slideshow">
		<div class="row">
			<?php $this->widget('booster.widgets.TbCarousel',
				array(
					'items'=>Slide::model()->getSlideForCarousel()
				)
			);?>
		</div>
	</div><!-- slideshow -->
	<?php } ?>
	
	<div id="content">
		<div class="row">
			<?php echo $content; ?>
		</div>
	</div><!-- content -->
	
	<div id="top-button">
		<div class="row">
			<?php $referralImg = CHtml::image(Yii::app()->baseUrl."/images/top.png","",array("class"=>"top-button"));
					echo CHtml::link($referralImg,'#top'); ?>
		</div>
	</div><!-- top button -->
	
	<div id="footer">
		<div class="row">
			<div class="container">
				<div id="content-footer" class="col-lg-4 col-md-4">
					<h4>CONTENT</h4>
					<ul>
						<li><?php print CHtml::link("PHILOSOPHY",array("page/read","id"=>2)); ?></li>
						<li><?php print CHtml::link("VIDEOGRAPHY",array("page/read","id"=>3)); ?></li>
						<li><?php print CHtml::link("PHOTOGRAPHY",array("page/read","id"=>4)); ?></li>
						<li><?php print CHtml::link("GRAPHIC DESIGN",array("page/read","id"=>5)); ?></li>
						<li><?php print CHtml::link("ILLUSTRATION",array("page/read","id"=>6)); ?></li>
						<li><?php print CHtml::link("WEB DESIGN",array("page/read","id"=>7)); ?></li>
						<li><?php print CHtml::link("CONTACT",array("page/read","id"=>8)); ?></li>
					</ul>
				</div>
				<div id="drop-a-line" class="col-lg-4 col-md-4">
					<h4>DROP A LINE</h4>
					<?php $this->renderPartial('//site/contact',array('model'=>new ContactForm));?>
				</div>
				<div id="contact" class="col-lg-4 col-md-4">
					<ul>
						<li><?php print CHtml::image(Yii::app()->baseUrl."/images/facebook.png"); ?></li>
						<li><?php print CHtml::image(Yii::app()->baseUrl."/images/twitter.png"); ?></li>
						<li><?php print CHtml::image(Yii::app()->baseUrl."/images/vimeo.png"); ?></li>
					</ul>
					<p>
					PT. ATLAS PRIMACO <br>
					Jl. Pasirlayung Selatan V/No 9<br>
					Bandung - 40192<br>
					INDONESIA
					</p>
					<p style="color:#dcaf47;;">
					Ph. +62 22 70 44 44 35<br><br>
					+62 819 10 0 91720<br>
					+62 813 2251 7335
					</p>
					<p>
					ruangjiwo@gmail.com<br>
					www.ruangjiwo-creativeworks.com
					</p>
				</div>
			</div>
				<div id="copyright">Copyright &copy; <?php echo date('Y'); ?></div>
		</div>
	</div><!-- footer 3 column -->

</div><!-- page -->
</body>
</html>
