<?php
$this->breadcrumbs=array(
	'Portfolio Images'=>array('index'),
	'Manage',
);
?>

<h1>Kelola Gambar Portofolio</h1>

<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'primary',
			'icon'=>'plus white',
			'label'=>'Tambah',
			'url'=>array('portfolioImage/create')
		)); ?>&nbsp;
		
<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'primary',
			'icon'=>'list white',
			'label'=>'Portofolio',
			'url'=>array('portfolio/admin')
		)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'portfolio-image-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'portfolio_id',
		'file',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
