<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'portfolio-image-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<p class="help-block">Kolom dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php print $form->labelEx($model,'file'); ?>
	<?php if(!empty($model->file)) print CHtml::image(Yii::app()->request->baseUrl.'/uploads/portfolioImage/'.$model->file); ?>
	<?php echo $form->fileField($model,'file',array('class'=>'span5','maxlength'=>255)); ?>
	<?php print $form->error($model,'file'); ?>
	
	<div>&nbsp;</div>
	
<div class="form-actions">
	<?php print CHtml::submitButton('Save'); ?>
	<?php /* $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
		)); */ ?>
</div>

<?php $this->endWidget(); ?>
