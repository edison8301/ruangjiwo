<?php

class PortfolioImageController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view','directDelete'),
'users'=>array('@'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate($portfolio_id)
	{
		$model=new PortfolioImage;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PortfolioImage']))
		{
			$model->attributes=$_POST['PortfolioImage'];
			$model->portfolio_id=$portfolio_id;
			$file = CUploadedFile::getInstance($model,'file');
			
			if($file!==null)
				$model->file = str_replace(' ','-',time().'_'.$file->name);
			
			if($model->save())
			{
				if($file!==null)
				{
					$path = Yii::app()->basePath.'/../uploads/portfolioImage/';
					$file->saveAs($path.$model->file);
				}
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('portfolio/view','id'=>$model->portfolio_id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$old_thumbnail = $model->file;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PortfolioImage']))
		{
			$model->attributes=$_POST['PortfolioImage'];
			
			$file = CUploadedFile::getInstance($model,'file');
			
			if($file!==null)
				$model->file = str_replace(' ','-',time().'_'.$file->name);
			else
				$model->file = $old_thumbnail;
			
			if($model->save())
			{
				if($file!==null)
				{
					$path = Yii::app()->basePath.'/../uploads/portfolioImage/';
					$file->saveAs($path.$model->file);
					
					if(file_exists($path.$old_thumbnail) AND $old_thumbnail!='')
						unlink($path.$old_thumbnail);
				}
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('portfolio/view','id'=>$model->portfolio_id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionDirectDelete($id)
	{
		$model = $this->loadModel($id);
		
		$portfolio_id = $model->portfolio_id;
		$file = $model->file;
		
		if($model->delete())
		{
			$path = Yii::app()->basePath.'/../uploads/portfolioImage/';
					
			if(file_exists($path.$file) AND $file!='')
				unlink($path.$file);
		}
		
		Yii::app()->user->setFlash('success','Data berhasil dihapus');
		$this->redirect(array('portfolio/view','id'=>$portfolio_id));
		
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('PortfolioImage');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new PortfolioImage('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['PortfolioImage']))
$model->attributes=$_GET['PortfolioImage'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=PortfolioImage::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='portfolio-image-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
